class signup{

    getfirstname(){
        return cy.get('[name="fname"]')
    }

    getlastname(){
        return cy.get('[name="lname"]')
    }

    getemail(){
        return cy.get('#Signuppopup > .sign-in-txtbox > [type="email"]')
    }

    getpassword(){
        return cy.get('#Signuppopup > .sign-in-txtbox > [type="password"]')
    }

    getsignupbutton(){
        return cy.get('#Signuppopup > .sign-in-txtbox > .login-btn')
    }



}
export default signup